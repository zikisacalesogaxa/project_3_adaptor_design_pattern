import * as $ from "jquery";
const apiV1 = 'https://placesofinterest.io/api/v1/places';
const apiV2 = 'http://167.99.36.10:8080/api/v2/places/';

export default class Adaptor {

    private baseUrl: any;

    constructor(version: number) {

        // check which version is passed
        if (version === 1) {
            this.baseUrl = apiV1
        } else if (version === 2) {
            this.baseUrl = apiV2;
        }

    }

    public getAllPlaces = () => {

        $.getJSON(this.baseUrl, (data: any) => {
            return data;
        });

    }

    public getPlacesByCategory = (category: string) => {

        $.getJSON(this.baseUrl + `/category/${category}/`, (data: any) => {
            return data;
        });

    }

    public getPlaceById = (placeId: number) => {

        $.getJSON(this.baseUrl + `/${placeId}`, (data: any) => {
            return data;
        });

    }

    public sendReview = (userId: number, placeId: number, review: string) => {

        $.ajax({
            url: this.baseUrl + `/${userId}/review/${placeId}`,
            data: JSON.stringify(review),
            type: "POST", contentType: "application/json",
            success: (data) => {
                return data;
            }
        });

    }

    public planTrip = (userId: number, placeId: number) => {

        $.ajax({
            url: this.baseUrl + `/${userId}/plan-trip/${placeId}`,
            type: "POST", contentType: "application/json",
            success: (data) => {
                return data;
            }
        });

    }

    public plannedTrips = (userId: number) => {

        $.getJSON(this.baseUrl + `/${userId}/planned-trips`, (data: any) => {
            return data;
        });

    }

    public getUser = (userId: number) => {

        $.getJSON(this.baseUrl + `/${userId}`, (data: any) => {
            return data;
        });

    }

}
